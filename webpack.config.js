const path = require('path');
const webpack = require('webpack');

const PATHS = {
  build: path.join(__dirname, 'build')
};

module.exports = {
    entry: "./app/app.js",
    output: {
        path: PATHS.build,
        filename: "bundle.js"
    },
    node: {
        fs: "empty"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style-loader!css-loader" },
            { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'},
            { test: /pixi.js/, loader: "script" },
            { test: /phaser.js/, loader: "script" }
        ]
    },
    devServer: {
        contentBase: PATHS.build,
        historyApiFallback: true,
        hot: true,
        inline: true,
        progress: true,
        stats: 'errors-only',
        host: process.env.HOST,
        port: process.env.PORT
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};