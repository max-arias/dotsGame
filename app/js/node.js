Node = function (game, rotateSpeed) {

    Phaser.Sprite.call(this, game, game.world.randomX, game.world.randomY, 'planet1');

    this.anchor.setTo(0.5, 0.5);

    this.rotateSpeed = rotateSpeed;

    game.add.existing(this);

};

Node.prototype = Object.create(Phaser.Sprite.prototype);
Node.prototype.constructor = Node;

Node.prototype.update = function() {
    this.angle += this.rotateSpeed;
};

module.exports = Node;