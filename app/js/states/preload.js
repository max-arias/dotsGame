var Preload = function(game){}

Preload.prototype = {
    preload: function(){ 
      /*var loadingBar = this.add.sprite(160, 240, "loading");
      loadingBar.anchor.setTo(0.5, 0.5);
      this.load.setPreloadSprite(loadingBar);*/

      this.game.load.image("planet1", "assets/meteorBrown_small1.png");
      this.game.load.image("planet2", "assets/meteorGrey_small1.png");
    },
    create: function(){
        this.game.state.start("gametitle");
    }
}

module.exports = Preload;