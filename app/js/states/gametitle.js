var GameTitle = function(game){}

GameTitle.prototype = {
    create: function(){
        var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center"};

        //  The Text is positioned at 0, 100
        text = this.game.add.text(0, 0, "Test Text", style);
        text.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
        text.setTextBounds(0, 20, 800, 0);


        textStart = this.game.add.text(0, 300, "Start", style);
        textStart.setShadow(3, 3, 'rgba(100,0,0,0.5)', 2);
        textStart.setTextBounds(0, 20, 800, 0);

        textStart.inputEnabled = true;
        textStart.events.onInputDown.add(this.playTheGame, this);
    },
    playTheGame: function(){
        this.game.state.start("main");
    }
}

module.exports = GameTitle;