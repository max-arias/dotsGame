require('./lib/pixi.js');
require('./lib/phaser.js');
require('./css/app.css');

var boot      = require('./js/states/boot');
var preload   = require('./js/states/preload');
var gametitle = require('./js/states/gametitle');
var main      = require('./js/states/main');
var gameover  = require('./js/states/gameover');

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game_div', null, true);

game.state.add("boot", boot);
game.state.add("preload", preload);
game.state.add("gametitle", gametitle);
game.state.add("main", main);
game.state.add("gameover", gameover);

game.state.start("boot");